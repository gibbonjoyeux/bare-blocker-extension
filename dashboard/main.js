//////////////////////////////// BY CACTUSFLUO /////////////////////////////////

"use strict";

window.browser = window.browser || window.chrome || window.msBrowser;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

const	REGEX_URL		= /https?:\/\/[\w\.\-@:%_\+~#=]+/i;

let		g_dom_whitelist	= null;
let		g_dom_list		= null;
let		g_dom_i_add		= null;
let		g_whitelist		= null;
let		g_sites			= null;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

function		remove_site_from_storage(url) {
	delete g_whitelist[url];
	browser.storage.local.set({"whitelist": g_whitelist});
}

function		create_remove_button(dom_list, dom_site, url) {
	let			dom;

	dom = document.createElement("input");
	dom.type = "button";
	dom.value = "REMOVE";
	dom.name = "test";
	dom.addEventListener("click", function () {
		dom_list.removeChild(dom_site);
		remove_site_from_storage(url);
	});
	dom_site.appendChild(dom);
}

function		create_title(dom_site, url) {
	let			dom;

	dom = document.createElement("a");
	dom.href = url;
	dom.target = "_blank";
	dom.textContent = url;
	dom_site.appendChild(dom);
}

function		handle_site(dom_list, url) {
	let			dom;

	dom = document.createElement("li");
	create_remove_button(dom_list, dom, url);
	create_title(dom, url);
	dom_list.appendChild(dom);
}

function		handle_sites(data) {
	let			dom;
	let			sites, site;

	if (g_dom_list != null) {
		g_dom_whitelist.removeChild(g_dom_list);
	}
	dom = document.createElement("ul");
	sites = Object.keys(g_whitelist);
	for (site of sites) {
		handle_site(dom, site);
	}
	g_dom_whitelist.appendChild(dom);
	g_dom_list = dom;
}

function		handle_add(event) {
	let			url;

	/// IF KEY IS NOT ENTER
	if (event.type == "keyup" && event.keyCode != 13) {
		return;
	}
	url = REGEX_URL.exec(g_dom_i_add.value)
	if (url != null) {
		g_whitelist[url[0].toLowerCase()] = true;
		/// UPDATE PAGE
		handle_sites();
		/// UPDATE STORAGE
		browser.storage.local.set({"whitelist": g_whitelist});
	}
}

document.addEventListener('DOMContentLoaded', function() {
	let		dom_b_add;

	g_sites = [];
	/// GET DOCUMENTS
	g_dom_whitelist = document.getElementById("whitelist");
	g_dom_i_add = document.getElementById("i_add");
	dom_b_add = document.getElementById("b_add");
	/// BUILD PAGE
	browser.storage.local.get("whitelist", function (data) {
		g_whitelist = data.whitelist || {};
		/// HANDLE SITES
		handle_sites();
		/// HANDLE ADD SITE
		g_dom_i_add.addEventListener("keyup", handle_add);
		dom_b_add.addEventListener("click", handle_add);
	});
});
