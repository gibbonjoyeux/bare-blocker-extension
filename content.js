//////////////////////////////// BY CACTUSFLUO /////////////////////////////////

"use strict";

window.browser = window.browser || window.chrome || window.msBrowser;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

let		g_port			= null;
let		g_images		= null;
let		g_to_update		= null;

const	IMAGE_PATH		= browser.runtime.getURL("simple.svg");

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

function		on_message(activation) {
	let			object;

	/// MODE START
	if (activation != null) {
		/// IF ACTIVATED
		if (activation == true) {
			/// PRELOAD IMAGES
			browser.storage.local.get("mode", preload_images);
			return;
		} else {
			g_port.disconnect();
			return;
		}
	}
	/// MODE UPDATE IMAGES
	/// DEQUEUE
	while (g_to_update.length > 0) {
		object = g_images[g_to_update.shift()];
		object.image.src = object.src;
		object.image.removeEventListener("mousehover", object.event);
	}
}

function		prepare_image_to_update(i) {
	/// TELL BACKGROUND NOT TO STOP THIS URL
	g_port.postMessage(g_images[i].src);
	/// ADD IMAGE TO QUEUE
	g_to_update.push(i);
	return;
}

function		preload_images(data) {
	let			images, image;
	let			event_func;
	let			i;

	/// EXIT IF MODE OFF
	if (data.mode == false) {
		return;
	}
	g_to_update = [];
	g_images = [];
	/// GET IMAGES
	images = document.getElementsByTagName("img");
	/// FOR EACH IMAGE
	for (i = 0; i < images.length; ++i) {
		image = images[i];
		event_func = (function (i) {
			return (function () {
				prepare_image_to_update(i);
			});
		})(i);
		/// CREATE NEW IMAGE OBJECT
		g_images.push({
			"event": event_func,
			"image": image,
			"src": image.src
		});
		image.src = IMAGE_PATH;
		image.srcset = "";
		image.addEventListener("mouseover", event_func);
	}
}

////////////////////////////////////////////////////////////////////////////////
/// RUN
////////////////////////////////////////////////////////////////////////////////

g_port = browser.runtime.connect();
g_port.onMessage.addListener(on_message);
