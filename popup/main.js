//////////////////////////////// BY CACTUSFLUO /////////////////////////////////

"use strict";

window.browser = window.browser || window.chrome || window.msBrowser;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

const	REGEX_URL		= /https?:\/\/[\w\.\-@:%_\+~#=]+/;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// WHITELIST
//////////////////////////////////////////////////

function		init_whitelist(dom_whitelist, dom_button, dom_button_tmp,
				whitelist, url) {
	if (url == null) {
		dom_whitelist.textContent = "";
		return;
	}
	/// IF WHITELISTED
	if (url in whitelist) {
		dom_button.value = "REMOVE";
		dom_button_tmp.style.display = "none";
		return (true);
	/// IF NOT WHITELISTED
	} else {
		dom_button.value = "ADD";
		return (false);
	}
}

function		add_whitelist_event(dom_button, dom_button_tmp, whitelist,
				url, listed) {
	dom_button.addEventListener("click", function () {
		/// IF WHITELISTED
		if (listed == true) {
			delete whitelist[url];
			dom_button.value = "ADD";
			dom_button_tmp.style.display = "inline";
		/// IF NOT WHITELISTED
		} else {
			whitelist[url] = true;
			dom_button.value = "REMOVE";
			dom_button_tmp.style.display = "none";
			browser.tabs.reload();
		}
		/// UPDATE
		browser.storage.local.set({"whitelist": whitelist});
		listed = !listed;
	});
}

function		handle_whitelist(whitelist, dom_whitelist_tmp) {
	browser.tabs.query({"active": true, "currentWindow": true}, function (tab) {
		let		dom_whitelist;
		let		dom_button, dom_button_tmp;
		let		url;
		let		listed;

		dom_whitelist = document.getElementById("whitelist");
		dom_button = document.getElementById("b_whitelist");
		dom_button_tmp = document.getElementById("b_whitelist_tmp");
		url = REGEX_URL.exec(tab[0].url);
		url = (url == null) ? null : url[0];
		/// INIT
		listed = init_whitelist(dom_whitelist, dom_button, dom_button_tmp,
		whitelist, url);
		/// HANDLE CLICK
		add_whitelist_event(dom_button, dom_button_tmp, whitelist, url,
		listed);
		/// WHITELIST TMP EVENT
		dom_button_tmp.addEventListener("click", function () {
			browser.runtime.sendMessage({});
		});
	});
}

//////////////////////////////////////////////////
/// MODE
//////////////////////////////////////////////////

function		update_mode(dom_mode, mode) {
	dom_mode.value = (mode) ? "ON" : "OFF";
	browser.storage.local.set({"mode": mode});
}

function		handle_mode(mode) {
	let			dom_mode;

	dom_mode = document.getElementById("b_mode");
	update_mode(dom_mode, mode);
	dom_mode.addEventListener("click", function () {
		mode = !mode;
		update_mode(dom_mode, mode);
	});
}

//////////////////////////////////////////////////
/// RUN
//////////////////////////////////////////////////

document.addEventListener('DOMContentLoaded', function() {
	/// HANDLE WHITELIST & MODE
	browser.storage.local.get(["whitelist", "mode"], function (data) {
		handle_mode(data.mode);
		handle_whitelist(data.whitelist || {});
	});
	/// HANDLE DASHBOARD
	document.getElementById("b_dashboard").addEventListener("click",
	function () {
		browser.tabs.create({"url": "./dashboard/index.html"}, null);
	});
});
