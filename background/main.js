//////////////////////////////// BY CACTUSFLUO /////////////////////////////////

"use strict";

window.browser = window.browser || window.chrome || window.msBrowser;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

const	REGEX_URL		= /https?:\/\/[\w\.\-@:%_\+~#=]+/;
const	IMAGE_PATH		= browser.runtime.getURL("simple.svg");

let		g_mode			= true;
let		g_to_update		= {};
let		g_whitelist		= {};
let		g_whitelist_tmp	= {};

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

function		get_base_url(url) {
	let			match;

	match = REGEX_URL.exec(url);
	if (match == null) {
		return (null);
	} else {
		return (match[0]);
	}
}

function		on_request(details) {
	/// EXIT IF MODE OFF
	if (g_mode == false) {
		return;
	}
	/// EXIT IF WHITELISTED URL
	if (g_whitelist[details.initiator] == true
	|| g_whitelist_tmp[details.initiator] == true) {
		return;
	}
	/// IF NOT BLOCKER IMAGE
	if (details.url != IMAGE_PATH) {
		/// IF NOT RELOAD
		if (g_to_update[details.url] == null) {
			/// CANCEL REQUEST
			return ({"cancel": true});
		/// ELSE
		} else {
			delete g_to_update[details.url];
		}
	}
}

function		on_connection(port) {
	let			url;

	/// HANDLE TAB CONTENT INITIALIZATION
	url = get_base_url(port.sender.url);
	/// NO URL
	if (url == null) {
		port.postMessage(true);
	/// IF WHITELISTED
	} else if (g_whitelist[url] == true) {
		port.postMessage(false);
	/// IF TEMPORARY WHITELISTED
	} else if (g_whitelist_tmp[url] == true) {
		port.postMessage(false);
	/// IF UNLISTED
	} else {
		port.postMessage(true);
	}
	/// HANDLE IMAGE UPDATING MESSAGES
	port.onMessage.addListener(function (message) {
		g_to_update[message] = true;
		port.postMessage(null);
	});
}

////////////////////////////////////////////////////////////////////////////////
/// RUN
////////////////////////////////////////////////////////////////////////////////

/// IMAGES & FONTS
if ("IMAGESET" in browser.webRequest.ResourceType) {
	browser.webRequest.onBeforeRequest.addListener(on_request,
	{"urls": ["*://*/*"], "types": ["image", "imageset", "font"]},
	["blocking"]);
} else {
	browser.webRequest.onBeforeRequest.addListener(on_request,
	{"urls": ["*://*/*"], "types": ["image", "font"]}, ["blocking"]);
}
/// MESSAGES
browser.runtime.onConnect.addListener(on_connection);
/// INIT
browser.browserAction.setIcon({"path": "icons/16_on.png"});
browser.runtime.onInstalled.addListener(function () {
	browser.storage.local.set({"mode": true});
});
