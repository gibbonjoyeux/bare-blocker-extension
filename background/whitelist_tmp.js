//////////////////////////////// BY CACTUSFLUO /////////////////////////////////

"use strict";

window.browser = window.browser || window.chrome || window.msBrowser;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

function		on_tab_remove() {
	browser.tabs.query({}, function (tabs) {
		let		new_whitelist;
		let		base_url;
		let		tab;

		new_whitelist = {};
		for (tab of tabs) {
			base_url = get_base_url(tab.url);
			if (g_whitelist_tmp[base_url] == true) {
				new_whitelist[base_url] = true;
			}
		}
		g_whitelist_tmp = new_whitelist;
	});
}

function		on_whitelist_tmp() {
	browser.tabs.query({"active": true, "currentWindow": true}, function (tab) {
		let		url;

		url = get_base_url(tab[0].url);
		if (url != null) {
			g_whitelist_tmp[url] = true;
			browser.tabs.reload();
		}
	});
}

////////////////////////////////////////////////////////////////////////////////
/// RUN
////////////////////////////////////////////////////////////////////////////////

/// WHITELIST TMP
browser.runtime.onMessage.addListener(on_whitelist_tmp);
browser.tabs.onRemoved.addListener(on_tab_remove);
